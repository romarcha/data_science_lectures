# Data Science Lectures Repository

## Description
This repository contains lectures asociated with data-science,
which contain a variety of topics and case studies with
relevant code and data related to each problem.

## Dependencies
The programming language used for this lectures is Python,
specifically Python 3.7.

The package dependencies are the following:

```
sudo pip install pandas
sudo pip install numpy
sudo pip install matplotlib
sudo pip install pystan
```

