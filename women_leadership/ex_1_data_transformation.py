#!/usr/bin/env python

#Example Data Transformation over Women Leadership
__author__ = "Roman Marchant and Sally Cripps"
__copyright__ = "Copyright (C) 2017, CTDS"
__license__ = "GPL"
__version__ = "0.0.1"
__maintainer__ = "Roman Marchant"
__email__ = "roman.marchant@sydney.edu.au"

#Import Generic Modules
import matplotlib.pyplot as plt
import numpy as np

#Import Custom Functions and Objects
from load_and_preprocess import load_and_preprocess
from models.stan_bayesian_linear_regression import StanBayesianLinearRegression

data_path = 'data/women_leadership.csv'
data = load_and_preprocess(data_path,grouping="None")

#Train Data
t = data['t']
y = data['y']
#Log-Log transformation
log_t = np.log(t)
log_y = np.log(y)

#Fit a linear regression and use Maximum A Posteriori
dimensions = 1

model = StanBayesianLinearRegression(dimensions,['time'],mode='Optimising',iterations=10000)
model.train_and_test(t,y)
alpha_t =  model.fit['alpha']
beta_t =  model.fit['beta']
residuals = model.fit['y_train_pred'].reshape(len(y),1)-y
#Line for fit
t_0 = 0
y_0 = alpha_t
t_1 = np.max(t)
y_1 = beta_t*(t_1 - t_0) + y_0


model_log = StanBayesianLinearRegression(dimensions,['time'],mode='Optimising',iterations=10000)
model_log.train_and_test(log_t,log_y)
alpha_log_t =  model_log.fit['alpha']
beta_log_t =  model_log.fit['beta']
log_residuals = model_log.fit['y_train_pred'].reshape(len(log_y),1)-log_y
#Line for fit
log_t_0 = 0
log_y_0 = alpha_log_t
log_t_1 = np.max(log_t)
log_y_1 = beta_log_t*(log_t_1 - log_t_0) + log_y_0

#Plot Raw Data
plt.figure()
plt.plot(t,y,alpha=.7,marker='o',linewidth=0)
plt.axis([np.min(t)-1,np.max(t)+1,np.min(y),np.max(y)])
plt.title('Percentage of Women (Ungrouped) over Time')
plt.xlabel('Number of Years since 1996')
plt.ylabel('Percentage of Woman')
plt.grid(True)
#plt.show()

#Plot Plot Transformed Data
plt.figure()
plt.plot(log_t,log_y,alpha=.7,marker='o',linewidth=0)
plt.title('Log-Log Percentage of Women (Ungrouped) over Time')
plt.xlabel('Log(Number of Years since 1996)')
plt.ylabel('Log(Percentage of Woman)')
plt.grid(True)
#plt.show()

#Plotting of Data and fit.
f, axarr = plt.subplots(1,2)
f.suptitle("Data Transformation", fontsize=16)
axarr[0].plot(t,y,alpha=.7,marker='o',linewidth=0)
axarr[0].set_title('Raw Data')
axarr[0].set_xlabel('Time (t)')
axarr[0].set_ylabel('Percentage of Women (y)')
axarr[1].plot(log_t,log_y,alpha=.7,marker='o',linewidth=0)
axarr[1].set_title('Log-Log Data')
axarr[1].set_xlabel('Log Time (log(t))')
axarr[1].set_ylabel('Log Percentage of Women (log(y))')
#Plot model fit
axarr[0].plot([t_0, t_1], [y_0, y_1], c='r',linewidth=2)
axarr[1].plot([log_t_0, log_t_1], [log_y_0, log_y_1], c='r',linewidth=2)
#plt.show()

#Plotting of Residual Histograms.
f, axarr = plt.subplots(1,2)
f.suptitle("Histogram of Residuals", fontsize=16)
axarr[0].hist(residuals.flatten(), 100, normed=1, alpha=0.75)
axarr[0].set_title('Raw Data')
axarr[1].hist(log_residuals.flatten(), 100, normed=1, alpha=0.75)
axarr[1].set_title('Log-Log Data')
plt.show()