#!/usr/bin/env python

#Example Stan Bayesian Linear Regression over Women Leadership
__author__ = "Roman Marchant and Sally Cripps"
__copyright__ = "Copyright (C) 2017, CTDS"
__license__ = "GPL"
__version__ = "0.0.1"
__maintainer__ = "Roman Marchant"
__email__ = "roman.marchant@sydney.edu.au"

#Import Generic Modules
import matplotlib.pyplot as plt
import numpy as np

#Import Custom Functions and Objects
from load_and_preprocess import load_and_preprocess
from models.stan_bayesian_linear_regression import StanBayesianLinearRegression

data_path = 'data/women_leadership.csv'
data = load_and_preprocess(data_path,grouping="None")

#Train Data
t = data['t']
y = data['y']
#Log-Log transformation
log_t = np.log(t)
log_y = np.log(y)

#Test Data
n_test = 100
log_t_test = np.linspace(np.min(log_t), np.max(log_t), num=n_test).reshape((n_test,1))

"""
#Plot of raw data
plt.figure()
plt.plot(log_t,log_y,alpha=.7,marker='o',linewidth=0)
plt.axis([np.min(log_t)-1,np.max(log_t)+1,np.min(log_y),np.max(log_y)])
plt.title('Log-Log Percentage of Women (Ungrouped) over Time')
plt.xlabel('Log(Number of Years since 1996)')
plt.ylabel('Log(Percentage of Woman)')
plt.grid(True)
plt.show()
"""

#Dimensions corresponds to the dimensionality of X, in this case it is 1, which is time.
dimensions = 1
model = StanBayesianLinearRegression(dimensions,['time'],iterations=50000)
model.train_and_test(log_t,log_y,log_t_test)
model.plot_results()
model.plot_predictive(dimension = 0)
