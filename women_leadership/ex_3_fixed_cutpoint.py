#!/usr/bin/env python

#Example Stan Bayesian Linear Regression with fixed cutpoint over Women Leadership
__author__ = "Roman Marchant and Sally Cripps"
__copyright__ = "Copyright (C) 2017, CTDS"
__license__ = "GPL"
__version__ = "0.0.1"
__maintainer__ = "Roman Marchant"
__email__ = "roman.marchant@sydney.edu.au"

#Import Generic Modules
import matplotlib.pyplot as plt
import numpy as np

#Import Custom Functions and Objects
from load_and_preprocess import load_and_preprocess
from models.stan_bayesian_linear_regression import StanBayesianLinearRegression

data_path = 'data/women_leadership.csv'
data = load_and_preprocess(data_path,grouping="None")

#2010 Cutpoint, corresponds to t = 15
cutpoint = 15

#Train Data
t = data['t']
y = data['y']
#Log-Log transformation
log_t = np.log(t)
log_y = np.log(y)

#X matrix contains log_t,L_i,L_i*t
binary_over_cutpoint = (t > cutpoint).astype(int)
log_t_over_cutpoint = np.multiply(log_t,(t > cutpoint).astype(int))
X = np.hstack((log_t,binary_over_cutpoint,log_t_over_cutpoint))

#Test Data
n_test = 100
log_t_test = np.linspace(np.min(log_t), np.max(log_t), num=n_test).reshape((n_test,1))
test_binary_over_cutpoint = (np.exp(log_t_test) > cutpoint).astype(int)
log_t_test_over_cutpoint = np.multiply(log_t_test,(np.exp(log_t_test) > cutpoint).astype(int))
X_test = np.hstack((log_t_test,test_binary_over_cutpoint,log_t_test_over_cutpoint))

#Dimensions corresponds to the dimensionality of X, in this case it is 1, which is time.
dimensions = 3
model = StanBayesianLinearRegression(dimensions,['time','over cutpoint', 'time over cutpoint'],iterations=50000)
model.train_and_test(X,log_y,X_test)
model.plot_results()
model.plot_predictive(dimension=0)