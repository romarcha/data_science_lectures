import pandas as pd
import numpy as np

def load_and_preprocess(data_path,grouping = "None"):
    """
    Load and preprocess the women in leadership data.
    Keyword arguments:
    data_path -- Path to the raw data in csv.
    grouping -- The results can be grouped by: "None", "Company" or "Industry"
    Returns:
    data -- Postprocessed data frame.
    """

    #Load CSV file
    data = pd.read_csv(data_path)

    #Transform year to new variable 't' that starts at 1
    data['t'] = data['Year'] - min(data['Year']) + 1
    #Transform number of women in company to percentage
    data['y'] = 100*data['Women Directors']/data['Total Directors']

    if grouping == "None":
        data_dict = dict()
        #Reshaping to specific dimensionality is requested by stan. (Explicit one column)
        data_dict['t'] = data['t'].as_matrix().reshape(data.shape[0],1)
        data_dict['y'] = data['y'].as_matrix().reshape(data.shape[0],1)
        return data_dict
    elif grouping == "Industry":
        industry_names = data['Industry'].unique()
        data['Industry_idx'] = data['t']*0
        for i in range(0,len(industry_names)):
            data.loc[data['Industry']==industry_names[i],'Industry_idx'] = i

        data_dict = dict()
        #Reshaping to specific dimensionality is requested by stan. (Explicit one column)
        data_dict['t'] = data['t'].as_matrix().reshape(data.shape[0],1)
        data_dict['y'] = data['y'].as_matrix().reshape(data.shape[0],1)
        data_dict['industry_id'] = data['Industry_idx'].as_matrix().reshape(data.shape[0],1)
        data_dict['industry'] = data['Industry'].as_matrix().reshape(data.shape[0],1)

        return data_dict
    elif grouping == "Company Name":
        #Extract the number of unique company names (Number of individuals)
        company_names = data['Company Name'].unique()
        n_companies = len(company_names)

        # t is a unique array of time stamps
        t = data['t'].unique()
        # y is a matrix, with rows representing time and columns representing companies
        y = np.zeros((len(t),n_companies))

        for i in range(0,n_companies):
            company_name = company_names[i]
            company_data = data[data['Company Name'] == company_name].sort_values('t',ascending=True)
            #Verify for no missing data
            if (company_data.t.values == t).all():
                y[:,i] = company_data.y.values

        data_dict = dict()
        data_dict['t'] = t.reshape(len(t),1)
        data_dict['company_names'] = company_names
        data_dict['y'] = y
        return data_dict
    else:
        data_dict = dict()
        #Reshaping to specific dimensionality is requested by stan. (Explicit one column)
        data_dict['t'] = data['t'].as_matrix().reshape(data.shape[0],1)
        data_dict['y'] = data['y'].as_matrix().reshape(data.shape[0],1)
        return data_dict

data_path = 'data/women_leadership.csv'
data = load_and_preprocess(data_path)