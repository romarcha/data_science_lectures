#!/usr/bin/env python

from __future__ import division
import pystan
import pickle
from hashlib import md5
import numpy as np
import matplotlib.pyplot as plt
from scipy.stats.kde import gaussian_kde

__author__ = "Roman Marchant"
__copyright__ = "Copyright (C) 2017, CTDS"
__license__ = "GPL"
__version__ = "0.2.1"
__maintainer__ = "Roman Marchant"
__email__ = "roman.marchant@sydney.edu.au"

class StanBayesianLinearRegression:
    """Class that performs Bayesian Linear Regression on Stan """

    def __init__(self,dimensions,dimension_names,iterations = 2000, chains = 1, mode = "Sampling"):
        # Set the dimensionality of the input space
        self.dim = dimensions
        self.dimension_names = dimension_names
        self.iter = iterations
        self.chains = chains
        self.mode = mode

    def get_stan_model_code(self):
        stan_code = """
        data {
            int<lower=0> N; // number of data items
            int<lower=0> K; // number of predictors
            matrix[N, K] x; // predictor matrix
            vector[N] y;    // outcome vector

            int<lower=0> N_test;        // number of test items
            matrix[N_test, K] x_test;   // predictor test matrix
        }
        parameters {
            real alpha;             // intercept
            vector[K] beta;         // coefficients for predictors
            real<lower=0> sigma;    // error scale
        }
        model {
            y ~ normal(x*beta+alpha,sigma); // likelihood
        }
        generated quantities {
            vector[N_test] y_test;
            vector[N] y_train_pred;
            for(n in 1:N_test)
                y_test[n] = normal_rng(x_test[n]*beta+alpha,sigma);
            for(n in 1:N)
                y_train_pred[n] = normal_rng(x[n]*beta+alpha,sigma);
        }
        """
        return stan_code

    def train_and_test(self,X_train,y_train,X_test = []):
        self.X_train = X_train
        self.y_train = y_train
        if X_test == []:
            X_test = np.zeros((0,self.dim))
        self.X_test = X_test
        N = X_train.shape[0]
        K = X_train.shape[1]
        x = X_train
        y = y_train.flatten()
        N_test = X_test.shape[0]
        stan_data = {'N': N,
                   'K': K,
                   'x': x,
                   'y': y,
                   'N_test': N_test,
                   'x_test': X_test
                    }
        self.fit = self.stan_cache(model_code=self.get_stan_model_code(), mode=self.mode, data=stan_data, iter=self.iter, chains=self.chains)

    def calc_fit_summary_stats(self):
        self.summary_stats = []
        alpha = self.fit.extract(pars='alpha')['alpha']
        self.summary_stats.append(self.get_summary_stats(alpha,"Intercept"))

        for i in range(0,len(self.dimension_names)):
            beta_i = self.fit.extract(pars='beta')['beta'][:,i]
            self.summary_stats.append(self.get_summary_stats(beta_i,self.dimension_names[i]))

        sigma = self.fit.extract(pars='sigma')['sigma']
        self.summary_stats.append(self.get_summary_stats(sigma,"Sigma"))

    def get_summary_stats(self,chain_vals,name):
        param_stats = dict()
        param_stats["name"] = name
        param_stats["mean"] = np.mean(chain_vals)
        param_stats["std"] = np.std(chain_vals)
        param_stats["ci"] = (param_stats["mean"]-2*param_stats["std"],param_stats["mean"]+2*param_stats["std"])
        return param_stats

    def plot_results(self):
        alpha = self.fit.extract(pars='alpha')['alpha']
        kde = gaussian_kde(alpha)
        dist_space = np.linspace(np.min(alpha),np.max(alpha),100)
        f, axarr = plt.subplots(1,2,gridspec_kw = {'width_ratios':[1, 3]})
        f.suptitle("Alpha", fontsize=16)
        axarr[0].plot(dist_space,kde(dist_space))
        axarr[0].set_title('Density')
        axarr[0].grid(True)
        axarr[1].plot(alpha)
        axarr[1].set_title('Raw Samples from Chain')
        axarr[1].grid(True)

        #plt.show()

        beta = self.fit.extract(pars='beta')['beta']
        #Quick solve when the dimensionality is 1.
        if self.dim == 1:
            kde = gaussian_kde(beta)
            dist_space = np.linspace(np.min(beta),np.max(beta),100)
            f, axarr = plt.subplots(1,2,gridspec_kw = {'width_ratios':[1, 3]})
            f.suptitle("Beta", fontsize=16)
            axarr[0].plot(dist_space,kde(dist_space))
            axarr[0].set_title('Density')
            axarr[0].grid(True)
            axarr[1].plot(beta)
            axarr[1].set_title('Raw Samples from Chain')
            axarr[1].grid(True)
        else:
            f, axarr = plt.subplots(self.dim,2,gridspec_kw = {'width_ratios':[1, 3]})
            f.suptitle("Beta", fontsize=16)
            for i in range(0,self.dim):
                kde = gaussian_kde(beta[:,i])
                dist_space = np.linspace(np.min(beta[:,i]),np.max(beta[:,i]),100)
                axarr[i,0].plot(dist_space,kde(dist_space))
                axarr[i,0].set_title('Density')
                axarr[i,0].grid(True)
                axarr[i,1].plot(beta[:,i])
                axarr[i,1].set_title('Raw Samples from Chain')
                axarr[i,1].grid(True)
        #plt.show()

    def plot_predictive(self,dimension = []):
        if self.dim == 1 or dimension != []:
            y_test = self.fit.extract(pars='y_test')['y_test']
            mean_vector = np.zeros((y_test.shape[1],1))
            std_vector = np.zeros((y_test.shape[1],1))
            for i in range(0,y_test.shape[1]):
                mean_vector[i] = np.mean(y_test[:,i])
                std_vector[i] = np.std(y_test[:,i])
            plt.figure()
            plt.plot(self.X_train[:,dimension],self.y_train,alpha=0.5,marker='.',linewidth=0)
            plt.plot(self.X_test[:,dimension],mean_vector,alpha=1,marker='.')
            plt.fill_between(self.X_test[:,dimension].flatten(), (mean_vector+2*std_vector).flatten(), (mean_vector-2*std_vector).flatten(),alpha=0.2)
            plt.title('Predictive Distribution')
            plt.grid(True)
            plt.show()
        else:
            print("Predictive distribution plot not available for dim > 1")


    def stan_cache(self, model_code, mode, model_name=None, **kwargs):
        """Use just as you would `stan`"""
        code_hash = md5(model_code.encode('ascii')).hexdigest()
        if model_name is None:
            cache_fn = 'cached-model-{}.pkl'.format(code_hash)
        else:
            cache_fn = 'cached-{}-{}.pkl'.format(model_name, code_hash)
        try:
            sm = pickle.load(open(cache_fn, 'rb'))
        except:
            sm = pystan.StanModel(model_code=model_code)
            with open(cache_fn, 'wb') as f:
                pickle.dump(sm, f)
        else:
            print("Using cached StanModel")
        if mode == 'Sampling':
            return sm.sampling(**kwargs)
        elif mode == 'Optimising':
            kwargs.pop('chains', None)
            return sm.optimizing(**kwargs)